﻿<#
.SYNOPSIS

.VERSION 
  
.AUTHOR 
    Bart Tacken - Client ICT Groep
.PREREQUISITES
    PowerShell v2 (default on Server 2008)
.EXAMPLE
    NA
#>
param([string]$SQLServerList) #=$(Throw "Parameter missing: -SQLServerList ConfigGroup"))
#---------------------------------------------------------[Initialisations]---------------------------------------------------
Import-Module Send-Mail -Force -DisableNameChecking # Import custom Send Mail module and ignore warnings


#----------------------------------------------------------[Declarations]----------------------------------------------------------
# Mail
[string]$SMTPserverStr = 'MAIL01.dolmans.local'
[string]$SenderMailAddStr = 'NoReply@dolmans.nl'
[string]$RecepientMailAddStr = 'btacken@client.nl'


#-----------------------------------------------------------[Functions]------------------------------------------------------------
#-----------------------------------------------------------[Execution]------------------------------------------------------------
$SQLServerList = 'C:\Beheer\Scripts\EverWork-Reporting\SQLservers.txt'

Function Get-CPUInfo{
    [CmdletBinding()]
    Param(
    [parameter(Mandatory = $TRUE,ValueFromPipeline = $TRUE)]   [String] $ComputerName

    )

    Process{
    
            # Get Default SQL Server instance's Edition
            $sqlconn = new-object System.Data.SqlClient.SqlConnection(`
                        "server=$ComputerName;Trusted_Connection=true");
            $query = "SELECT SERVERPROPERTY('Edition') AS Edition;"

            $sqlconn.Open()
            $sqlcmd = new-object System.Data.SqlClient.SqlCommand ($query, $sqlconn);
            $sqlcmd.CommandTimeout = 0;
            $dr = $sqlcmd.ExecuteReader();

            while ($dr.Read()) { 
             $SQLEdition = $dr.GetValue(0);}

            $dr.Close()
            $sqlconn.Close()

   
            #Get processors information            
            $CPU=Get-WmiObject -ComputerName $ComputerName -class Win32_Processor
            #Get Computer model information
            $OS_Info=Get-WmiObject -ComputerName $ComputerName -class Win32_ComputerSystem
            
     
           #Reset number of cores and use count for the CPUs counting
           $CPUs = 0
           $Cores = 0
           
           foreach($Processor in $CPU){

           $CPUs = $CPUs+1   
           
           #count the total number of cores         
           $Cores = $Cores+$Processor.NumberOfCores
        
          } 
           
           $Script:InfoRecord = New-Object -TypeName PSObject -Property @{
                    Server = $ComputerName;
                    Model = $OS_Info.Model;
                    CPUNumber = $CPUs;
                    TotalCores = $Cores;
                    SQLEdition = $SQLEdition;
                    'Cores to CPUs Ratio' = $Cores/$CPUs;
                    Resume = if ($SQLEdition -like "Developer*") {"N/A"} `
                        elseif ($Cores -eq $CPUs) {"No licensing changes"} `
                        else {"licensing costs increase in " + $Cores/$CPUs +" times"};
    }
   Write-Output $InfoRecord
          }
}


#loop through the server list and get information about CPUs, Cores and Default instance edition
Get-Content $SQLServerList | Foreach-Object {Get-CPUInfo $_ }|Format-Table -AutoSize Server, Model, 
   SQLEdition, CPUNumber, TotalCores, 'Cores to CPUs Ratio'

$InfoRecordStr = $script:InfoRecord | Out-String
$script:c = $script:InfoRecord | Select @{N="Server Naam";E={$_.Server}},@{N="SQL versie";E={$_.SQLEdition}}`
,@{N="Aantal CPU's";E={$_.CPUNumber}},TotalCores,@{N="Aantal Core's";E={$_.TotalCores}} | ConvertTo-Html -Head $Head #-PostContent ""This report was generated on : ", (Get-Date -Format G)"





#| ConvertTo-Html #,SQLEdition,@{Name="SQL versie";E={$_.SQLEdition}
#$b = $b + ($script:InfoRecord | Select SQLEdition,@{Name="SQL versie";E={$_.SQLEdition}})
#$b = $b | ConvertTo-Html
# ,CPUNumber,@{Name="Aantal CPU's";E={$_.CPUNumber}

Write-Output "B is $c"
 
# Sent mail to contact user that password reset has been processed
$SubjectStr = "SQL Rapportage Dolmans"
$BodyStr = @"
    <html>
    <body>
    <FONT FACE="verdana" SIZE="2">
    <div style="float:right" "text-align:left;">           
                    <BR>
                    Hallo,<BR><BR>
                                      
                    Onderstaande een overzicht van de CPU cores tbv de SQL licenties van Dolmans:
                    <BR>
                    $script:c
					<BR><BR><BR>

                    <FONT COLOR=#042B45>EverWork Mutatie systeem<BR><BR></FONT>
                         
                    <img src="http://www.client.nl/wp-content/uploads/2015/05/EverWork_logo.jpg" 
                        width="50" 
                        height="50" 
                        border="0" 
                        alt="EverWork">
                                
        </html>
"@                  # Needs to be aligned on the left
 #98bf21;

 $head=@"
<style>
@charset "UTF-8";

table
{
font-family:"Verdana", Verdana, Verdana, verdana;
border-collapse:collapse;
}
td
{
font-size:1em;
border:1px solid #9DD8D9;
padding:5px 5px 5px 5px;
}
th
{
font-size:1.1em;
text-align:center;
padding-top:5px;
padding-bottom:5px;
padding-right:7px;
padding-left:7px;
background-color:#9DD8D9;
color:#ffffff;
}
name tr
{
color:#F00000;
background-color:#9DD8D9;
}
</style>
"@

Send-Mail -SMTPserverStr $SMTPserverStr -SenderMailAddStr $SenderMailAddStr -RecepientMailAddStr $RecepientMailAddStr -SubjectStr $SubjectStr -BodyStr $BodyStr #-CCMailAddStr 'bart_tacken@hotmail.com' 











