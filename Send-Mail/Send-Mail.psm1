﻿<#
.SYNOPSIS

.VERSION 
  
.AUTHOR 
    Bart Tacken - Client ICT Groep
.PREREQUISITES
    PowerShell v2 (default on Server 2008)
.EXAMPLE
    NA
#>
Function Send-Mail {
    [CmdletBinding()]
    Param(  
            [Parameter(Mandatory=$True,
                      ValueFromPipeline=$True,
                      ValueFromPipelineByPropertyName=$True)]
            [string]$SMTPserverStr,

            [Parameter(Mandatory=$True,
                      ValueFromPipeline=$True,
                      ValueFromPipelineByPropertyName=$True)]
            [string]$SenderMailAddStr,

            [Parameter(Mandatory=$True,
                      ValueFromPipeline=$True,
                      ValueFromPipelineByPropertyName=$True)]  
            [string]$RecepientMailAddStr,
			
            [Parameter(Mandatory=$False,
                      ValueFromPipeline=$True,
                      ValueFromPipelineByPropertyName=$True)]  
            [string]$CCMailAddStr,						

            [Parameter(Mandatory=$True,
                      ValueFromPipeline=$True,
                      ValueFromPipelineByPropertyName=$True)]
            [string]$SubjectStr,
            
            [Parameter(Mandatory=$True,
                      ValueFromPipeline=$True,
                      ValueFromPipelineByPropertyName=$True)]
            [string]$BodyStr,

            [Parameter(Mandatory=$False,
                      ValueFromPipeline=$True,
                      ValueFromPipelineByPropertyName=$True)]
            [string]$Attach1Str,

            [Parameter(Mandatory=$False,
                      ValueFromPipeline=$True,
                      ValueFromPipelineByPropertyName=$True)]
            [string]$Attach2Str
    ) # End Param

#Try {
    [string]$DateStr = (Get-Date).ToString("s").Replace(":","-") # +"_" # Easy sortable date string
    
    #Start-Transcript ('c:\windows\temp\' + "$Datestr" + '_Send-Mail.log') -ErrorAction SilentlyContinue # Start logging, prevent already started messages
   
    # Compose message
    $message = new-object System.Net.Mail.MailMessage
    $message.From = $SenderMailAddStr
    $message.To.Add($RecepientMailAddStr)
    $message.IsBodyHtml = $True
    $message.Subject = $SubjectStr
    $message.body = $BodyStr
	$message.cc.Add($CCMailAddStr)
    #$msg.bcc.Add(...)
		   
    # Attachement #2 (If specified)
    If ($Attach1Str) {
        $attach1Obj = new-object Net.Mail.Attachment($Attach1Str)
        $message.Attachments.Add($Attach1Obj)           
    }
    Else { $Attach1Str = 'No attachement specified' }
            
    # Attachement #2 (If specified
    If ($Attach2Str) {
        $attach2Obj = new-object Net.Mail.Attachment($Attach2Str)
        $message.Attachments.Add($Attach2Obj)            
    }
    Else { $Attach2Str = 'No attachement specified' }

    # Send Mail
    $smtp = new-object Net.Mail.SmtpClient($SMTPserverStr)
    $smtp.Send($message)

    # Log to transcript
    Write-Host ""
    Write-Host "Sending Email with following details:"
    Write-Host "Date/Time:     $(Get-Date)"
    Write-Host "Sender:        $SenderMailAddStr"
    Write-Host "Recipient:     $RecepientMailAddStr"
	Write-Host "CC:			   $CCMailAddStr"
    Write-Host "Subject:       $SubjectStr"
    #Write-Host "Body:          $BodyStr "
    Write-Host "Attachement 1: $Attach1Str "
    Write-Host "Attachement 2: $Attach2Str "
#Stop-Transcript -ErrorAction SilentlyContinue # Stop logging, prevent already started messages
} # End transcript
